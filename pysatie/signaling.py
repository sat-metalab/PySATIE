from blinker import Signal
from typing import Dict, Optional

class Signaling:
    """ An class containing signals emitted by PySATIE """

    def __init__(self) -> None:
        self._names = [
            'plugin_list_updated',
            'satie_heartbeat',
            'satie_get_synth_value',
            'satie_get_synth_parameters',
            'satie_get_configuration',
            'satie_get_status'
        ]
        # we use annonymus Signal() to unsure that they are all unique
        self._signals = {name: Signal() for name in self._names}

    @property
    def signals(self) -> Dict[str, Signal]:
        """A dictionary containing all available signals"""
        return self._signals
