"""OSC server using oscpy."""
import logging
import pickle
from os import path

from oscpy.server import OSCThreadServer
from oscpy.server import ServerClass
from oscpy import client
from pysatie.config import OSCconfig

from typing import Any, TYPE_CHECKING

if TYPE_CHECKING:
    from pysatie.satie import Satie

PREFIX = "/satie"
SCENE_PREFIX = "/satie/scene"
RENDERER_PREFIX = "/satie/renderer"

logger = logging.getLogger(__name__)


def callback(*args):
    """Report all OSC arguments."""
    print(args)


@ServerClass
class OSCserver(OSCThreadServer):
    """The osc server class."""

    def __init__(self, satie: 'Satie') -> None:
        """Initialize things."""
        self._satie: Satie = satie
        self._port: int = OSCconfig.port
        self._satie_host: str = OSCconfig.satie_host
        self._satie_port: int = OSCconfig.satie_port
        self._scserver_host: str = OSCconfig.scserver_host
        self._scserver_port: int = OSCconfig.scserver_port

        try:
            super().__init__(encoding='utf8')
            self.listen(address=self._satie_host, port=self._port, default=True)
        except RuntimeError as e:
            print("Failed: ", e)

    def load_synth_types(self) -> None:
        """Request plugin/synths types.

        :return:
        """
        self.send(path.join(PREFIX, "plugins"), [])

    def load_properties(self, plugin: str) -> None:
        """Send a request to SATIE for synth properties.

        :param plugin: Plugin source name
        :return:
        """
        self.send(path.join(PREFIX, "plugindetails"), [plugin])

    def send(self, message, args) -> None:
        """Send an OSC message."""
        msg = []
        [msg.append(arg) for arg in args]
        client.send_message(message, msg, self._satie_host, self._satie_port, encoding='utf8')

    ########################################
    # SC server level commands (bypass SATIE)

    def new_synth(self, synthDef: str, synthID: int, addAction: int = 0, targetID: int = 1, *args: Any):
        """Create a new synth directly on SuperCollider server.

        This command bypasses SATIE. It is here to facilitate interaction with
        the running SC server without being affected by SATIE's I/O configuration.
        Its purpose is to create test sounds before we're ready to work with
        SATIE. Use with caution.
        """
        arguments = locals()
        msg = []
        [msg.apped(v) for k, v in arguments.items() if k not in ['self', 'args']]
        [msg.append(m) for m in args]
        client.send_message("/s_new", msg, self._scserver_host, self._scserver_port)

    def set_synth_params(self, synthID: int, *args):
        """Control Synths directly on the server.

        This command bypasses SATIE altogether. It is meant to be used
        with Synths created manually by self.new_synth() method above.
        Useful for testing audio channels correspondence and calibrating.
        """
        msg = [synthID]
        [msg.append(m) for m in args]
        client.send_message("/s_new", msg, self._scserver_host, self._scserver_port)

    ########################################
    # configuration and boot messages
    def satie_boot(self) -> None:
        """Boot SATIE.

        This method does not return
        """
        self.send(path.join(PREFIX, "boot"), [])

    def satie_quit(self) -> None:
        """Quit SATIE.

        This method does not return
        """
        self.send(path.join(PREFIX, "quit"), [])

    def satie_reboot(self) -> None:
        """Reboot SATIE, or boot it if not currently booted.

        This method does not return
        """
        self.send(path.join(PREFIX, "reboot"), [])

    def satie_configure(self, configuration: str) -> None:
        """Configure SATIE by sending it a stringified JSON configuration.

        This method does not return
        """
        self.send(path.join(PREFIX, "configure"), [configuration])

    def get_satie_server_option(self, option: str) -> None:
        """Ask SATIE for the value of a server option in the current instance.

        This method does not return
        """
        self.send(path.join(PREFIX, "server", "options", "get"), [option])

    def get_satie_configuration(self) -> None:
        """Request a SatieConfiguration JSON.

        This method does not return
        """
        self.send(path.join(PREFIX, "configuration", "get"), [])

    def get_satie_status(self) -> None:
        """Request a string containing Satie status.

        This method does not return
        """
        self.send(path.join(PREFIX, "status"), [])

    ########################################
    # SATIE level messages

    def load_sample(self, name: str, filepath: str) -> None:
        """Load a sample from file into a buffer and register it with SATIE.

        Currently limited to file formats handled by SuperCollider (.wav, .aif(f))
        This method does not return

        :param name: unique name.
        :param filepath: absolute path to the soundfile.
        """
        self.send(path.join(PREFIX, "loadSample"), [name, filepath])

    ########################################
    # scene messages

    def scene_clear(self) -> None:
        """Clear the SATIE scene.

        This method does not return.
        """

        self.send(path.join(SCENE_PREFIX, "clear"), [])

    def scene_set(self, key: str, value: Any) -> None:
        """Set a scene-wide keyword value."""
        self.send(
            path.join(SCENE_PREFIX, "set"), [key, value])

    ########################################
    # Scene level interaction

    def set_postprocessor_property_value(self, key: str, value: Any, postproc_type: str = "postproc") -> None:
        """Set a single property on a post-processor.

        :param key: name of the property
        :param val: property's value
        :param postproc_type: type of postproc, either ``postproc`` (default) or ``ambiposproc``

        A post-processor is a singleton containing a pipeline of synths.
        """
        if postproc_type == 'postproc' or postproc_type == 'ambipostproc':
            self.send(
                path.join(SCENE_PREFIX, postproc_type, "set"), [key, value])
        else:
            logger.warning("Wrong value for postproc_type, should be postproc or ambiposproc")

    def set_postprocessor_property_vector(self, key: str, postproc_type: str = "postproc", *args: Any) -> None:
        """Set a post-processor property with a vector.

        :param key: name of the property
        :param *args: a sequence of values
        :param postproc_type: type of postproc, either ``postproc`` (default) or ``ambiposproc``
        """
        if postproc_type == 'postproc' or postproc_type == 'ambipostproc':
            msg = [key]
            [msg.append(arg) for arg in args]
            self.send(
                path.join(SCENE_PREFIX, postproc_type, "setarray"), msg)
        else:
            logger.warning("Wrong value for postproc_type, should be postproc or ambiposproc")

    def apply_postprocessor(self, postproc_type: str = "postproc") -> None:
        """Apply the configured post-processor.

        :param postproc_type: type of postproc, either ``postproc`` (default) or ``ambiposproc``
        """
        if postproc_type == 'postproc' or postproc_type == 'ambipostproc':
            self.send(
                path.join(SCENE_PREFIX, postproc_type, "apply"),[])
        else:
            logger.warning("Wrong value for postproc_type, should be postproc or ambiposproc")

    def set_postprocessor_pipeline_property(self, name: str, *args: Any) -> None:
        """Set a property of the pipeline.

        :param name: name of the postprocessor (project dependent)
        :param *args: key, value pairs for properties to affect
        """
        msg = [name]
        [msg.append(arg) for arg in args]
        self.send(
            path.join(SCENE_PREFIX, "postproc", "prop", "set"), msg)

    def load_properties(self, plugin: str) -> None:
        """Send a request to SATIE for synth properties.

        :param plugin: Plugin source name
        :return:
        """
        self.send(path.join(PREFIX, "plugindetails"), [plugin])

    def set_responder_destination(self, ip: str, port: int) -> None:
        """Tell SATIE's inspector to send to this address:port.

        :param ip: IP address
        :param port: port
        """
        self.send(path.join(PREFIX, "responder"), [ip, port])

    def create_source_group(self, name: str) -> None:
        """Create an audio source group.

        :param name: group name
        """
        self.send(path.join(SCENE_PREFIX, "createSourceGroup"), name)

    def create_effect_group(self, name: str) -> None:
        """Create an audio effect group.

        :param name: FX group name
        """
        self.send(
            path.join(SCENE_PREFIX, "createEffectGroup"), [name])

    def create_process_group(self, name: str) -> None:
        """Create an audio process group.

        :param name: Process group name
        """
        self.send(
            path.join(SCENE_PREFIX, "createProcessGroup"), [name])

    def create_source(self, node_id: str, source_name: str, group: str = 'default', **kwargs) -> None:
        """Instantiate an audio generator.

        :param node_id: instance id, must be unique
        :param source_name: synthdef's name
        :param group: the name of the group the instance will be in
        """
        msg = [node_id, source_name, group]
        [(msg.append(k), msg.append(v)) for k, v in kwargs.items()]
        self.send(
            path.join(SCENE_PREFIX, "createSource"), msg)

    def create_effect(self, node_id: str, effect_name: str, group: str = "defaultFX", auxbus: int = 0) -> None:
        """Instantiate an audio generator.

        :param node_id: instance id, must be unique
        :param effect_name: synthdef's name
        :param group: the name of the group the instance will be in
        :param auxbus: index of the effects bus
        """
        self.send(
            path.join(SCENE_PREFIX, "createEffect"),
            [node_id, effect_name, group, auxbus]
        )

    def create_process(self, node_id: str, process_name: str, *args, **kwargs) -> None:
        """Create/instantiate a process.

        :param node_id: instance id, must be unique
        :param process_name: name of the defined process
        :param *args: any arguments required to instantiate the process
        """
        msg = [node_id, process_name]
        [msg.append(arg) for arg in args]
        [(msg.append(k), msg.append(v)) for k, v in kwargs.items()]
        self.send(path.join(SCENE_PREFIX, "createProcess"), msg)

    def delete_node(self, node_name: str) -> None:
        """Delete a node form the scene.

        :param node_name:
        :return:
        """
        self.send(path.join(SCENE_PREFIX, "deleteNode"), [node_name])

    def set_orientation(self, aziDeg: float, eleDeg: float) -> None:
        """Set renderer's orientation offset.

        :params aziDeg: azimuth in degrees
        :params eleDeg: elevation in degrees
        """
        self.send(path.join(RENDERER_PREFIX, "setOrientationDeg"), [aziDeg, eleDeg])

    def set_output_db(self, outputVolume: float) -> None:
        """Set renderer's output volume.

        :params outputVolume: gain in dB (between -90 and +6)
        """
        self.send(path.join(RENDERER_PREFIX, "setOutputDB"),
                  [max(min(outputVolume, 6), -90)])

    def set_dim(self, state: bool) -> None:
        """Set renderer's dim state.

        :params state: 0/1
        """
        self.send(path.join(RENDERER_PREFIX, "setOutputDim"), [int(state)])

    def set_mute(self, state: bool) -> None:
        """Set renderer's output state.

        :params state: 0/1
        """
        self.send(path.join(RENDERER_PREFIX, "setOutputMute"), [int(state)])

    def set_debug(self, debug: int) -> None:
        """
        Turn printing debug messages (in SuperCollider) on/off

        :params debug: 0/1
        """
        self.send(path.join(SCENE_PREFIX, "debug"), [debug])

    def enable_heartbeat(self, flag: bool) -> None:
        """Turn SATIE's status heartbeat on/off.

        :params flag: 0/1
        """
        self.send(path.join(PREFIX, "heartbeat"), [int(flag)])

    ########################################
    # Node messages

    def node_update(self,
                    node_type: str,
                    node_name: str,
                    azimuth: float,
                    elevation: float,
                    gain: float,
                    delay: float,
                    lp: float) -> None:
        """
        Update node properties.

        :param node_type: source/effect or process
        :param node_name: named instance
        :param azimuth: horizontal angle degrees
        :param elevation: vertical angle degrees
        :param gain: decibels
        :param delay: milliseconds
        :param lp: low-pass filter frequency herz
        :param distance: meters
        """
        self.send(
            path.join(PREFIX, node_type, "update"),
            [
                node_name,
                azimuth,
                elevation,
                gain,
                delay,
                lp
            ]
        )

    def node_set(self, node_type: str, node_name: str, *args: Any) -> None:
        r"""
        Set properties.

        :param node_type: either source, group or process
        :param node_name: instance name
        :param *args: key, value pairs alternating
        """
        msg = [node_name]
        [msg.append(arg) for arg in args]
        for i in range(len(msg)):
            if type(msg[i]) is list:
                msg[i] = bytearray(pickle.dumps(i))
        print(msg)
        self.send(
            path.join(PREFIX, node_type, "set"), msg)

    def node_set_vector(self, node_type: str, node_name: str, key: str, *args: Any) -> None:
        """
        Set property.

        :param node_type: either source, group or process
        :param node_name: instance name
        :param key: parameter name
        :param args:  sequence of values
        """
        msg = [node_name, key]
        [msg.append(arg) for arg in args]
        self.send(
            path.join(PREFIX, node_type, "setvec"), msg)

    def node_state(self, node_type: str, node_name: str, state: int) -> None:
        """
        Set state (DSP computation).

        :param node_type: either source, group or process
        :param node_name: instance name
        :param state: (int) 1 = active, 0 = inactive
        """
        self.send(
            path.join(PREFIX, node_type, "state"), [node_name, state])

    def node_event(self, node_type: str, node_name: str, event_name: str, *args: Any) -> None:
        r"""
        Execute an event associated with a node.

        :param node_type: either source, group or process
        :param node_name: instance name
        :param event_name: event name (usually a method defined by source, effect or process)
        :param \*args: optional parameters required by the event
        """
        msg = [node_name, event_name]
        [msg.append(arg) for arg in args]
        self.send(
            path.join(PREFIX, node_type, "event"), msg)

    def get_synth_parameters(self, synth: str, group: str) -> None:
        """Request SATIE's running synth parameters.

        This method does not return
        """
        self.send(
            path.join(PREFIX, "source", "parameters", "get"), [synth, group])

    def get_synth_value(self, key: str, synth: str, group: str) -> None:
        """Request the value of a given parameter of a running synth."""
        self.send(
            path.join(PREFIX, "source", "get"), [synth, key, group])

    ########################################################################
    # Process messages

    def process_property(self, node_name: str, *args: Any) -> None:
        r"""
        Set a property of a process.

        :param node_name: process instance name
        :param \*args: a sequence of key/value pairs
        """
        msg = [node_name]
        [msg.append(arg) for arg in args]
        self.send(
            path.join(PREFIX, "process", "set"), msg)

    def process_eval(self, node_name: str, handler: str, *args: Any) -> None:
        r"""
        Evaluate a process method.

        :param node_name: process instance name
        :param handler: name of the method to be evaluated
        :param \*args: any arguments required for the method
        """
        msg = [node_name, handler]
        [msg.append(arg) for arg in args]
        self.send(
            path.join(PREFIX, "process", "eval"), msg)
