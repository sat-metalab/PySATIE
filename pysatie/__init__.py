__author__ = "Michał Seta"
__copyright__ = "Copyright 2017, Société des arts technologiques"
__credits__ = ["Nicolas Bouillot", "François Ubald Brien", "Emmanuel Durand", "Michał Seta", "Jérémie Soria"]
__license__ = "GPLv3"
__maintainer__ = "Michał Seta"
__email__ = "metalab@sat.qc.ca"
__status__ = "Development"

__version__ = "0.3.0"
