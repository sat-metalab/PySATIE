import os
import sys

from dataclasses import dataclass, field
from pathlib import Path


@dataclass
class OSCconfig:
    """
    Dataclass for configuration of some of the OSC parameters
    """
    host: str = "localhost"
    port: int = 18060
    satie_host: str = "localhost"
    satie_port: int = 18032
    scserver_host: str = satie_host
    scserver_port: int = 57110


@dataclass
class Platform:
    """
    Dataclass handling platoform shared and specific configurations
    """

    default_satie_config: str = "default.scd"

    @property
    def name(self):
        return sys.platform

    @property
    def user_home_dir(self):
        return Path.home()

    @property
    def pysatie_path(self) -> str:
        return os.path.dirname(__file__)

    @property
    def satie_config_path(self):
        path = Path(self.pysatie_path).resolve()
        return os.path.join(str(path), 'data', Platform.default_satie_config)


class LinuxPlatform(Platform):
    SCSERVER_CMD = "scsynth"
    SUPERNOVA_CMD = "supernova"
    SCLANG_CMD = "sclang"


class WindowsPlatform(Platform):
    SCSERVER_CMD = "scsynth.exe"
    SUPERNOVA_CMD = "supernova.exe"
    SCLANG_CMD = "sclang.exe"


class DarwinPlatform(Platform):
    EXEC_PATH = "/Applications/SuperCollider/SuperCollider.app/Contents/MacOS/"
    SCSERVER_CMD = "scsynth"
    SUPERNOVA_CMD = "supernova"
    SCLANG_CMD = os.path.join(EXEC_PATH, "sclang")
