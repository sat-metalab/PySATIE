#+TITLE: Interacting with SATIE via PySATIE python module
#+OPTIONS: H:1 num:nil toc:t

#+BEGIN_SRC emacs-lisp :results none
(setq org-confirm-babel-evaluate nil)
(setq org-confirm-elisp-link-function nil)
#+END_SRC
* Contents                                                              :TOC:
- [[#introduction][Introduction]]
  - [[#optional-requirements][Optional requirements]]
  - [[#starting-up][Starting up]]
- [[#interacting-with-the-engine][Interacting with the engine]]
- [[#processes][Processes]]
- [[#post-processors][Post-processors]]
  - [[#post-processor-pipeline][Post-processor pipeline]]
  - [[#ambisonic-post-processor-pipeline][Ambisonic post-processor pipeline]]
  - [[#interactions-with-post-processors][Interactions with post-processors]]
- [[#quit-satie][Quit satie]]

* Introduction
This short guide assumes that you have successfully installed [[https://gitlab.com/sat-metalab/SATIE][SATIE]] by following its [[https://gitlab.com/sat-metalab/SATIE/blob/master/INSTALL-SC.md][documentation]]. Also, you will need to install this module, [[https://gitlab.com/sat-metalab/PySATIE][PySATIE]], the instructions are in its README.md file.

** Optional requirements
This document is in [[https://orgmode.org][OrgMode]] format which, when opened in correctly configured Emacs, allows for direct execution of code examples. However, this is not required. One can access this file directly in the /tutorial/ directory at https://gitlab.com/sat-metalab/PySATIE, where it will be cleanly rendered by your webrowser and the code can be easily tested via copy-paste method. Choose whichever method you prefer.

If you're familiar with Emacs/OrgMode combo, here are the prerequisites to execute directly from Emacs buffer:

- Jupyter (on Ubuntu: `sudo apt install jupyter-nobook`)
- ipython3
- [[https://github.com/gregsexton/ob-ipython][ob-ipython]] (for Python code blocks execution)
- [[https://github.com/djiamnot/ob-sclang][ob-sclang]] (for SuperCollider code blocks execution)

** Starting up
Once everything is installed, we start a SATIE server (example below is taken from SATIE quark documentation):

#+begin_src sclang :results none
(
s = Server.supernova.local;
~satieConfiguration = SatieConfiguration(s, [\stereoListener]);
~satie = Satie(~satieConfiguration);
~satie.waitForBoot({
    s.meter;
    s.makeGui;
    s.plotTree;
})
)
#+end_src

The above command will launch SATIE with a stereo speaker layout, 4 mono live inputs and a minimal graphical interface: meter levels, server tree display (where we can see if audio instances are being created) and it will also print verbose debug messages in the terminal.

#+name: pysatie_boot
#+begin_src jupyter-python :session satie :results none :exports code
from pysatie.satie import Satie

satie = Satie()
satie.initialize()
#+end_src


*** SATIE life cycle

We can quit SATIE:

#+begin_src jupyter-python :session satie :results none :exports code
satie.quit()
#+end_src

Quitting SATIE means simply quitting its server. The =Satie= and =SatieConfiguration= objects is not removed.

And we can boot it again. It will boot with the previous configuration:

#+begin_src jupyter-python :session satie :results none :exports code
satie.boot()
#+end_src

Or we can simply reboot it:

#+begin_src jupyter-python :session satie :results none :exports code
satie.reboot()
#+end_src

One can also reconfigure it. Re-configuring SATIE will automatically reboot it.
#+begin_src jupyter-python :session satie :results output :exports code
import json
satie_conf = {
    "satie_configuration": {
        "server": {
            "name": "null",
            "supernova": "false"
        },
        "minOutputBusChannels": 2,
        "numAudioAux": 2,
        "ambiOrders": [ 1, 3 ],
        "listeningFormat": [ "stereoListener", "quadpanaz" ],
        "outBusIndex": [ 0, 2 ]
    }
}
satie.configure(json.dumps(satie_conf))
#+end_src

#+RESULTS:

* Interacting with the engine

  By default, SATIE sends responses back to the same host:port where the message originated. However, sometimes it is desirable to set the destination by hand:
~satie.osc.set_responder_address("localhost", satie.server_port)~

Query SATIE for audio sources. All available ~plugins~ are stored in plugins instance variable:

#+name: pysatie_query_audio
#+BEGIN_SRC jupyter-python :session satie  :results none  :exports code
sources = satie.query_audiosources()
#+END_SRC

Print the names:
#+name: pysatie_print_plugins
#+BEGIN_SRC jupyter-python :session satie  :results value :exports both
plugs = [plugin.name for plugin in satie.plugins if satie.plugins]
print(plugs)
#+END_SRC

#+RESULTS: pysatie_print_plugins
: []


Let's make some noise:
First, we instantiate some granular sounds

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
dust = satie.get_plugin_by_name('DustDust')
satie.create_source_node('dustey', dust, group="default") if dust else print("nope")
#+END_SRC

Or create the same source but by passing it some arguments
#+BEGIN_SRC jupyter-python :session satie :exports both :results none
dust = satie.get_plugin_by_name('DustDust')
satie.create_source_node('dustey', dust, group="default", gainDB=10, aziDeg=-90, density1=100) if dust else print("nope")
#+END_SRC

By default, SATIE synths are muted when instantiated. We give it a 3D location coordinates and send an update() message which will automatically unmute the audio object and give it some gain and orientation based on its location.

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
location = [2,1,1]
satie.nodes['dustey'].update(location)
#+END_SRC

In addition to update() message, which, behind the scenes, updates only parameters specific to the spatializer (azimuth, elevation, distance, gain, delay, and filtering) we can also access properties specific to the sound source. First we ask SATIE to provide us with a list of properties of the sound object that is of interest to us:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
  def get_plugin_properties(plug_name):
      return[
          [(prop.name, prop.type, prop.default_value) for prop in plugin.properties]
          for plugin in satie.plugins if plugin.name == plug_name]
#+END_SRC


#+BEGIN_SRC jupyter-python :session satie  :results raw drawer :exports code
get_plugin_properties('DustDust')
#+END_SRC

Will give the following output:
#+BEGIN_EXAMPLE
  [[('density2', 'Integer', 10),
  ('trimDB', 'Integer', 0),
  ('density1', 'Integer', 1)]]
#+END_EXAMPLE
:end:

and we set the parameters as such:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.nodes['dustey'].set('density1', 1, 'density2', 5)
#+END_SRC

Let's create another type of sound:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
drone = satie.get_plugin_by_name('misDrone')
satie.create_source_node('droney', drone, group="default") if drone else print("nope")
#+END_SRC
And we update its location like we did with dustey

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
location = [-2,1,1]
satie.nodes['droney'].update(location)
#+END_SRC

After a while, droney will expire, it is a kamikaze type object, it destroys itself after its envelope has reached 0.
Let's recreate it and set some of its properties. First, we print what's available:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none

get_plugin_properties('misDrone')
drone = satie.get_plugin_by_name('misDrone')
satie.create_source_node('droney', drone, group="default") if drone else print("nope")
satie.nodes['droney'].set('dur', 20, 'freq', 333)
location = [-2,1,1]
satie.nodes['droney'].update(location)
#+END_SRC

Setting duration again, before the sound stopped, will extend it by the specified amount

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.nodes['droney'].set('dur', 60, 'freq', 433)
#+END_SRC

There is one more way to control a sound object. Let's first instantiate a model of a plucked string:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
pluck = satie.get_plugin_by_name('zkarpluck1')
satie.create_source_node('pluckey', pluck, group="default") if pluck else print("nope")
location = [-2,2,0.5]
satie.nodes['pluckey'].update(location)
satie.node_set_vector('pluckey', 'note', 60, 1, 1)
satie.node_set('pluckey', 't_trig', 1, 'gainDB', -10)
#+END_SRC

Update message: ~node_name, azimuth, elevation, gain(dB), delay(ms), low pass Hz~

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.node_update('pluckey', 45, 20, -20, 0.1, 1000)
satie.node_set('pluckey', 't_trig', 1)
#+END_SRC

Let's investigate the pluck thing:

#+BEGIN_SRC jupyter-python :session satie :exports both :results raw drawer
get_plugin_properties('zkarpluck1')
#+END_SRC

#+BEGIN_EXAMPLE
  [[('fb', 'Integer', 2),
  ('i_blockDelay', 'Integer', 2),
  ('c3', 'Integer', 20),
  ('t_trig', 'Integer', 0),
  ('c1', 'Integer', 1),
  ('note', 'Array', [60, 1, 0.5]),
  ('trimDB', 'Integer', 0)]]
#+END_EXAMPLE
:end:

Note the t_trig property. Sending it a non-zero value, will trigger the sound:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.nodes['pluckey'].set('t_trig', 1, 'fb', 20, 'c3', 10, 'c1', 1, 'trimDB', -10)
#+END_SRC

We can also set properties via a method:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.node_set('pluckey', 't_trig', 1, 'fb', 20, 'c3', 10, 'c1', 1, 'trimDB', 0)
#+END_SRC

Since note takes a vector, we can set it aside. node_set_vector() method takes a param and all values following it will be passed to SATIE as a vector:

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.node_set_vector('pluckey', 'note', 69, 1, 1)
satie.node_set('pluckey', 't_trig', 1)
#+END_SRC

Let's remove pluckey

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.delete_node('pluckey')
#+END_SRC

Finally, we clean the scene

#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.clear_scene()
#+END_SRC

* Processes

Processes allow more complex behaviors while limiting OSC communications between 3D engine and SATIE server. Processes are arbitrary routines that can be associated with particle systems, granular-type sound events, patterns, sequences. They are not limited to generating sound (or other events), they can also be used for introspection, tracking of various interactions or sound features and reporting. The process will run completely independent of the 3D engine control via OSC, unless appropriate methods have been implemented or overriden.

The following examples need a compiled process. The process `testProcess` is taken from the /Simple Process Example/ in the SATIE documentation, provided here, for convenience:

#+begin_src sclang :results none
(
// process definition
~helloProcess = Environment.make({

    // public
    ~name;
    ~group;
    ~interval = 0.5;

    // private
    ~routine;

    ~makeRoutine = { |self|
        self.routine = Routine {
            // here the routine creates sound objects that free themselves when they become silent
            loop {
                var note = rrand(60, 63);
                var elev = Prand([-90, -60, 30, 0, 90]).asStream;

                // Kamikaze is a special case of synth generated by SATIE
                // it frees itself when its envelope falls to silence
                self.satieInstance.makeKamikaze(
                    synthDefName: \zkarpluck1,
                    group: self.group,
                    synthArgs: [
                        \t_trig, 1,
                        \gainDB, rrand(-40, -30),
                        \note, [note, 1, 0.5],
                        \aziDeg, rrand(-180,180),
                        \spread, 0.01,
                        \eleDeg, elev.next
                    ]
                );
                self.interval.wait;
            }
        }
    };

    ~start = { | self | self.routine.reset; self.routine.play };

    // setup and cleanup methods are mandatory, particularly for use via OSC
    // setup method should expect at least two arguments: name of the process and group.
    ~setup = { |self, nodeName, groupName|
        self.name = nodeName.asSymbol;
        self.group = groupName.asSymbol;
        self.makeRoutine;
        self.start;
    };

    ~cleanup = { |self| self.routine.stop };

});
)
#+end_src

Register the process with SATIE
#+begin_src sclang
~satie.makeProcess(\testProcess, ~helloProcess);
#+end_src

Instantiate the process
- it will automatically execute its setup() method
- it will also create a unique group for this Process
- it will also register it with Satie in processInstances dictionary
#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.create_process_node('plucks', 'testProcess')
#+END_SRC

Set a property of the process
#+begin_src jupyter-python :session satie :exports both :results none
satie.set_process_property('plucks', 'interval', 0.9)
#+end_src

Clean up the process (i.e. stop the ~routine~).
#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.eval_process_method('plucks', 'cleanup')
#+END_SRC

Start it again:
#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.eval_process_method('plucks', 'start')
#+END_SRC

Remove the process instance:
#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.delete_node('plucks')
#+END_SRC

We can also instantiate a process and set its properties in one command:
#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.create_process_node('plucks', 'testProcess', interval=0.3)
#+END_SRC

Remove the process instance again:
#+BEGIN_SRC jupyter-python :session satie :exports both :results none
satie.delete_node('plucks')
#+END_SRC

* Post-processors

Post-processors are the final stage of the audio processing pipeline in SATIE. They could typically be compressors, limiters and similar types of effects that need to be applied at the end of the processing chain. Since SATIE supports several types of spatialization techniques, non-HOA post-processors cannot be shared with HOA post-processors, however, they can be used at the same time.

For instance, in non-HOA pipeline, post-processor pipeline and spatialization are considered as two different stages:

#+begin_src dot :file img/satie_pipeline.png :exports results
  digraph satie_vbap{
  rankdir=LR;
  size="8,5";
  pproc [shape="box, "label="post-processor pipeline"];
  sources -> effects;
  effects -> pproc;
  sources -> pproc;
  processes -> pproc;
  pproc -> spatializer;
  }
#+end_src

#+RESULTS:
[[file:img/satie_pipeline.png]]

Conversely, in the case of HOA signals, effects and spatializer (technically, the decoder) share the same pipeline.

#+begin_src dot :file img/satie_hoa_pipeline.png :exports results

  digraph satie_stuff{
    rankdir=LR;
    size="8,5";
    subgraph cluster_0 {
    fx [label="HOA effects"];
    decoder;
    label="HOA pipeline"
    fx -> decoder;
    }

  }
  }

#+end_src

#+RESULTS:
[[file:img/satie_hoa_pipeline.png]]

Following are some examples:

** Post-processor pipeline

Setting up a post-processor is done in several stages. First we modify its properties and we `apply` when ready. Note, that the post-processor name is decided by SATIE but it's easy to predict:

#+BEGIN_SRC jupyter-python :session satie :exports code :results none
  satie.set_postprocessor_property_value("outputIndex", 0)
  satie.set_postprocessor_property_value("spatializerNumber", 0)
  satie.set_postprocessor_property_vector("pipeline", "postproc", "delay", "limiter")
  satie.apply_postprocessor("postproc")
#+END_SRC

** Ambisonic post-processor pipeline
   In order to use the ambisonic post-processor pipeline, we need to instruct SATIE to do ambisonic processing as it is not intialized by default.
   Let's reconfigure SATIE (this will reboot the SATIE server):

   #+begin_src sclang
   ~config = SatieConfiguration(s, ambiOrders: [1]);
   ~satie.reconfigure(~config);
   #+end_src

   We can change it back to its default =
   #+begin_src sclang
   ~config = SatieConfiguration(s, [\stereoListener]);
   ~satie.reconfigure(~config);
   #+end_src

We set up an ambisonic post-processor in the same way but we need to append =ambipostproc= to function parameters. So for our new setup, it will look like this:
#+BEGIN_SRC jupyter-python :session satie :exports code :results none
  satie.set_postprocessor_property_value("outputIndex", 0, "ambipostproc")
  satie.set_postprocessor_property_value("spatializerNumber", 0, "ambipostproc")
  satie.set_postprocessor_property_value("order", 1, "ambipostproc")
  satie.set_postprocessor_property_vector("pipeline", "ambipostproc", "Rotate", "HOABinaural")
  satie.apply_postprocessor("ambipostproc")
#+END_SRC

** Interactions with post-processors

In order to change some post-processor's parameters, address the post-processor pipeline by its name. The pipeline's name depends whether it is in normal panning or ambisonic mode. See SATIE documentation on Spatializers and Ambisonics for more details on the naming convention of post-processors.

VBAP pipeline can be changed like this:
#+BEGIN_SRC jupyter-python :session satie :exports code :results none
  satie.set_postprocessor_pipeline_property("post_proc_0", "limitDB", -10, "del", 0.78)
#+END_SRC

Ambisonic variety will have a different name, generated automatically.
#+BEGIN_SRC jupyter-python :session satie :exports code :results none
  satie.set_postprocessor_pipeline_property("ambipost__s0_o1", "limitDB", -10, "del", 0.78)
#+END_SRC

In SuperCollider, we can visualize the state, which should confirm the above change (this command relies on the Quark `NodeSnapshot`):
#+begin_src sclang
TreeSnapshot.get({ |snap| snap.postln})
#+end_src

#+begin_example
TreeSnapshot
  + Group: 0
     + Group: 1
       + Group: 1000
      + Group: 1001
      + Group: 1002
         + Synth(1005): ambipost__s1_o1s
            rotateYawDeg: 0.0
            rotatePitchDeg: 0.0
            rotateRollDeg: 0.0
      + Group: 1003
         + Synth(1004): post_proc_0s
            del: 0.77999997138977
            limitDB: -10.0

#+end_example

#+BEGIN_SRC jupyter-python :session satie :exports code :results none
  satie.set_postprocessor_pipeline_property("ambipost__s1_o1", "rotateYawDeg", 90)
#+END_SRC

Which will yield:
#+begin_example
TreeSnapshot
  + Group: 0
     + Group: 1
       + Group: 1000
      + Group: 1001
      + Group: 1002
         + Synth(1005): ambipost__s1_o1s
            rotateYawDeg: 90.0
            rotatePitchDeg: 0.0
            rotateRollDeg: 0.0
      + Group: 1003
         + Synth(1004): post_proc_0s
            del: 0.77999997138977
            limitDB: -10.0
#+end_example
* Quit satie
#+name: pysatie_disconnect
#+begin_src jupyter-python :session satie :results none
satie.disconnect()
#+end_src
