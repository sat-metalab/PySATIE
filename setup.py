#!/usr/bin/env python
"""
setup.py for PySATIE

pysatie (working title) is a python module to interact wit SATIE
"""
from setuptools import setup
import pysatie

setup(
    name="PySATIE",
    version=pysatie.__version__,
    description="SATIE python module",
    author="Michał Seta",
    author_email="mseta@sat.qc.ca",
    url="https://gitlab.com/sat-metalab/PySATIE",
    install_requires=['oscpy', 'blinker', 'psutil'],
    python_requires='>=3',
    package_data={"pysatie": ["py.typed", "data/*.scd", "data/synthdefs/*.scd"]},
    include_package_data = True,
    packages=["pysatie"],
    zip_safe=False,
)
