# PySATIE Changelog
## [0.3.0] - 2023-03-21
### New features
- replace liblo by python-only OSC module, oscpy
### Fixes
- some cleanup
## [0.2.0] - 2022-04-20
### New features
- Signaling via blinker module
- add support for renderer's volume, dim and mutestates
- add methods for instantiating and controling SynthDef on server
- High level methods for playing dac tester
- Launch sclang with a default SATIE config
- Handle querying server options
- Handle SatieConfiguration request/response
- DAC tester synthdefs separate from config

## [0.1.0] - 2021-02-23
### New features
- Support complete SATIE OSC API
